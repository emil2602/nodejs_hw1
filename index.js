const express = require('express');
const app = express();
const morgan = require('morgan');
const routerFile = require('./route/routerFile');
const cors = require('cors');
const serverPort = 8080;

app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());

app.use('/api/files', routerFile);

app.listen(serverPort, ()=> {
  console.log(`Server is runing on: ${serverPort}`);
})

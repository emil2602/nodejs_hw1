const path = require('path');
const fs = require('fs');

const validateFile = (req, res, next) => {
  const fileExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
  const { filename, content } = req.body;

  if (!filename || !filename.trim()) {
    return res.status(400).json({ message: "Please specify 'filename' parametr" })
  };

  if (fs.existsSync(`./files/${filename}`)) {
    return res.status(400).json({ message: `File already exists` })
  };

  if (!fileExtensions.includes(path.extname(filename))) {
      return res.status(400).json({ message: 'File is not allowed' });
  };

  if (!content) {
      return res.status(400).json({ message: "Please specify 'content' parametr" });
  };

  if (!fs.existsSync('./files')) {
    try {
      await fs.promises.mkdir('./files')
    } catch {
      console.log('Failed to create directory')
    }
  }


  next();

}

module.exports = validateFile;